import asyncio
import json
import logging
import os
from dataclasses import dataclass, asdict
from functools import partial
from io import BytesIO
from uuid import uuid4
from nats import connect
from nats.aio.msg import Msg
from nats.js.client import JetStreamContext
from nats.js.object_store import ObjectStore
from PIL import Image, ImageDraw, ImageFont
from openai import OpenAI
from dotenv import load_dotenv, find_dotenv

@dataclass
class OpenAIPayload:
    task_uid: str
    model: str
    messages: list
    temperature: float
    max_tokens: int
    top_p: float
    frequency_penalty: float
    presence_penalty: float
    response_format: str

@dataclass
class OpenAIResult:
    task_uid: str
    response: str
    completion_tokens: int
    prompt_tokens: int
    worker: str

def process_photo(image_bytes: bytes):
    image = Image.open(BytesIO(image_bytes))
    d1 = ImageDraw.Draw(image)
    font = ImageFont.truetype("roboto.ttf", 40)
    d1.text(xy=(10, 10), text="PRIVET MEDVED", font=font, fill=(255, 0, 0))
    buff = BytesIO()
    image.save(buff, format="JPEG")
    buff.seek(0)
    return buff.read()

logging.basicConfig(level=logging.INFO)

async def worker(msg: Msg, storage: ObjectStore, second_storage: ObjectStore, js: JetStreamContext):
    logging.info(f"msg from bot, {msg}")
    storage_info = await storage.get(name=msg.headers["uid_key"])
    processed_photo = process_photo(storage_info.data)
    #await storage.delete(name=msg.headers["uid_key"])
    uid_key = uuid4().hex
    await asyncio.sleep(5)
    headers = {
        "uid_key": uid_key,
        "user_id": msg.headers["user_id"],
        "Nats-Msg-Id": uid_key,
    }
    await second_storage.put(
        name=uid_key,
        data=processed_photo,
    )
    await js.publish("bot.photo.converter.out", headers=headers)
    await msg.ack()

async def openai_process(client: OpenAI, messages: list, model: str, temperature: float, max_tokens: int, top_p: float, frequency_penalty: float, presence_penalty: float, response_format: str, task_uid: str):
    response = client.chat.completions.create(
        model=model,
        messages=messages,
        temperature=temperature,
        max_tokens=max_tokens,
        top_p=top_p,
        frequency_penalty=frequency_penalty,
        presence_penalty=presence_penalty,
        response_format=response_format,
    )
    if not response.choices:
        return None
    else:
        response_ai = response.choices[0].message.content
        logging.info(f"OpenAI response: {json.dumps(response_ai)}")
        completion_tokens = response.usage.completion_tokens
        prompt_tokens = response.usage.prompt_tokens
        return {"response": json.dumps(response_ai), "task_uid": task_uid, "completion_tokens": completion_tokens, "prompt_tokens": prompt_tokens, "worker": "openai_worker_1"}

async def worker_openai(msg: Msg, storage: ObjectStore, second_storage: ObjectStore, js: JetStreamContext, openai_client: OpenAI):
    logging.info(f"msg from bot, {msg}")
    #storage_info = await storage.get(name=msg.headers["uid_key"])
    payload = OpenAIPayload(**json.loads(msg.data))
    openai_response = await openai_process(
        client=openai_client,
        messages=payload.messages,
        model=payload.model,
        temperature=payload.temperature,
        max_tokens=payload.max_tokens,
        top_p=payload.top_p,
        frequency_penalty=payload.frequency_penalty,
        presence_penalty=payload.presence_penalty,
        response_format=payload.response_format,
        task_uid=payload.task_uid
    )
    if openai_response is None:
        logging.error("OpenAI response is None")
    else:
        result_openai = OpenAIResult(
            task_uid=openai_response["task_uid"],
            response=openai_response["response"],
            completion_tokens=openai_response["completion_tokens"],
            prompt_tokens=openai_response["prompt_tokens"],
            worker="openai_worker_1")
        bytes_ = json.dumps(asdict(result_openai)).encode()
        logging.info(f"json: {bytes_}")
        await js.publish("openai.single-text.out", bytes_)
        await msg.ack()

async def main():
    load_dotenv(find_dotenv())
    client = OpenAI(api_key=os.getenv("OPENAI_API_KEY"))
    nc = await connect()
    js = nc.jetstream()
    storage = await js.object_store("photos")
    second_storage = await js.object_store("ready_photos")
    sub = await js.subscribe(
        "openai.single-text.in",
        durable="openai_worker_1",
        manual_ack=False,
        cb=partial(worker_openai, storage=storage, second_storage=second_storage, js=js, openai_client=client),
    )
    try:
        await asyncio.Event().wait()
    finally:
        await sub.unsubscribe()

asyncio.run(main())
