import asyncio
import json
import logging
import os
from dataclasses import dataclass, asdict
from functools import partial
from io import StringIO
import pandas as pd
from uuid import uuid4
from aiogram import Bot, Dispatcher, F
from aiogram.types import BufferedInputFile, Message
from nats import connect
from nats.aio.msg import Msg
from nats.js.client import JetStreamContext
from nats.js.object_store import ObjectStore
from dotenv import load_dotenv, find_dotenv

logging.basicConfig(level=logging.INFO)


@dataclass
class OpenAIResult:
    task_uid: str
    response: str
    completion_tokens: int
    prompt_tokens: int
    worker: str

@dataclass
class OpenAIPayload:
    task_uid: str
    model: str
    messages: list
    temperature: float
    max_tokens: int
    top_p: float
    frequency_penalty: float
    presence_penalty: float
    response_format: str

async def worker(msg: Msg, bot: Bot, storage: ObjectStore):
    result = OpenAIResult(**json.loads(msg.data))
    result_string = json.loads(result.response)
    with StringIO(result_string) as json_file:
        decoded_json = json.load(json_file)
        logging.info(f"Decoded JSON: {decoded_json}")
    df = pd.DataFrame()
    try:
        df = pd.json_normalize(decoded_json)
        df['completion_tokens'] = result.completion_tokens
        df['prompt_tokens'] = result.prompt_tokens
        df['task_uid'] = result.task_uid
        df['worker_id'] = result.worker
    except Exception as e:
        logging.info(f"Error: {e}")

    try:
        df_load = pd.read_csv("result.csv")
        df_final = pd.concat([df_load, df])
        df_final.to_csv("result.csv", index=False)
    except:
        logging.info("File not found")
        df.to_csv("result.csv", index=False)
    await msg.ack()

# async def get_photo(m: Message, bot: Bot, storage: ObjectStore, js: JetStreamContext):
#     file = await bot.download(m.photo[-1])
#     uid_key = uuid4().hex
#     await storage.put(
#         name=uid_key,
#         data=file.read(),
#     )
#     logging.info(f"photo saved, {uid_key}")
#     logging.info("Storage info: %s", {storage})
#     await js.publish(
#         subject="bot.photo.converter.in",
#         headers={
#             "user_id": str(m.from_user.id),
#             "uid_key": uid_key,
#             "Nats-Msg-Id": uid_key,
#         },
#     )

async def get_document(m: Message, bot: Bot, storage: ObjectStore, js: JetStreamContext):
    file = await bot.download(m.document)
    uid_key = uuid4().hex
    df = pd.read_csv(file)
    for index, row in df.iterrows():
        task_id = str(row['id'])
        payload_openai = OpenAIPayload(
            task_uid=1,
            model="gpt-3.5-turbo",
            messages=[
                {
                    "role": "system",
                    "content": "Ты будешь размечать отзыв с {ID} и текстом {TEXT} c оценкой {REVIEW_STARS} пользователя о продукте {PRODUCT} с размером {SIZE}. Отдай валидный JSON в качестве результата."
                },
                {
                    "role": "user",
                    "content": "Пришли JSON-файла следующей структуры:\n{\n    \"id\": string, // возьми идентификатор отзыва из исходны данных как есть\n    \"sentiment\": string, // Эмоциональная окраска отзыва - один из вариантов (positive / neutral / negative)\n    \"keywords_positive\":  string[], // Массив ключевых слов, характеризующий позитивные стороны товара\n    \"phrases_positive\": string[], // Массив фраз слов, характеризующий позитивные стороны товара\n    \"keywords_negative\": string[], // Массив ключевых слов, характеризующий негативную оценку и проблемы с товаром\n    \"phrases_negative\": string[], // Массив фраз слов, характеризующий негативные стороны товара\n    \"matching_size_flag\" : string, // подошел ли товар по размеру клиенту (True / False / None)\n    \"matching_price_flag\" : string, // соответствует ли товар цене по мнению покупателя(True / False / None)\n    \"matching_photo_flag\" : string, // соответствует ли товар фото, представленному в карточке товара по мнению клиента (True / False / None)\n    \"matching_description_flag\": string, // соответствует ли товар описанию в карточке товара в магазине по мнению клиента (True / False / None)\n    \"extracted_height\": string, // рост клиента из его отзыва\n    \"extracted_size\": string, // размер товара, если он указан в отзыв\n    \"most_valuable_characteristics\"  : string[], // Массив ключевых аспектов и характеристик товара, на основе которых клиент оценивал товар, что для данного клиента наиболее важно\n    \"improve_recommendation\": string, // 1-фразовая рекомендация, как улучшить продукт или процесс, если в тексте отзыва есть негатив\n    \"subject\": string // предмет из навазния товара  (например \"Юбка\") (1-3 слова)\n}\n---------\nВажные требования:\n1. Не использй предмет из PRODUCT в ключевых словах, так как это и так часто втречающаяся информация\n2. Сохраняй простоту навзаний при разметке.\n3. Используй только строчные буквы.\n4. Если товар - не одежда, то информация о размере может быть проигнорирована"
                },
                {
                    "role": "assistant",
                    "content": "Хорошо жду входную информацию"
                },
                {
                    "role": "user",
                    "content": f"ID = {task_id} | TEXT={row['text']}| REVIEW_STARS= {row['productValuation']} | PRODUCT = Юбка миди | SIZE = {row['size']}"
                }],
            temperature=0.7,
            max_tokens=1000,
            top_p=1.0,
            frequency_penalty=0.0,
            presence_penalty=0.0,
            response_format={"type": "json_object"}
        )
        logging.info(f"Payload_ID: {task_id}")
        bytes_ = json.dumps(asdict(payload_openai)).encode()
        await js.publish("openai.single-text.in", bytes_)

    # await storage.put(
    #     name=uid_key,
    #     data=file.read(),
    # )
    # logging.info(f"document saved, {uid_key}")
    # logging.info("Storage info: %s", {storage})
    # await js.publish(
    #     subject="bot.photo.converter.in",
    #     headers={
    #         "user_id": str(m.from_user.id),
    #         "uid_key": uid_key,
    #         "Nats-Msg-Id": uid_key,
    #     },
    # )
async def main() -> None:
    load_dotenv(find_dotenv())
    bot = Bot(os.getenv("BOT_API_KEY"), parse_mode="HTML")
    dp = Dispatcher()

    nc = await connect()
    js = nc.jetstream()

    #await js.create_object_store("photos")
    #await js.create_object_store("ready_photos")
    await js.create_object_store("openai_input_texts")
    await js.create_object_store("openai_output_texts")

    storage = await js.object_store("photos")
    storage_ready = await js.object_store("ready_photos")
    #dp.message.register(get_photo, F.photo)
    dp.message.register(get_document, F.document)
    sub = await js.subscribe(
        "openai.single-text.out",
        cb=partial(worker, bot=bot, storage=storage_ready),
    )
    try:
        await dp.start_polling(bot, storage=storage, js=js, skip_updates=True)
    finally:
        await sub.unsubscribe()

if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
